package com.paulconroy.gameproject

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.annotation.SuppressLint
import android.graphics.Point
import android.media.MediaPlayer
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.support.v7.widget.CardView
import android.view.*
import android.view.animation.BounceInterpolator
import android.view.animation.LinearInterpolator
import android.widget.*
import at.wirecube.additiveanimations.additive_animator.AdditiveAnimator
import com.paulconroy.gameproject.custom.BlockItem
import com.paulconroy.gameproject.custom.TargetItem
import java.util.*
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity() {

    private val SWIPE_THRESHOLD = 100
    private val SWIPE_VELOCITY_THRESHOLD = 100
    var dX: Float = 0.toFloat()
    var dY: Float = 0.toFloat()
    var windowWidth = 0
    lateinit var vBlock: BlockItem
    lateinit var flBoard: CardView
    lateinit var vColumnLeft: RelativeLayout
    lateinit var vColumnTwo: RelativeLayout
    lateinit var vColumnThree: RelativeLayout
    lateinit var vColumnFour: RelativeLayout
    lateinit var vColumnRight: RelativeLayout
    lateinit var tvJudge: TextView
    lateinit var tvCombbo: TextView
    lateinit var ivComboCircle: ImageView
    lateinit var ivComboCirclePulse: ImageView
    private var dXLock: Boolean = false
    private var dXInit: Float = 0.toFloat()
    private var targetTimer: Handler = Handler()
    private lateinit var columnList: ArrayList<RelativeLayout>
    private var height = 0
    private var score = 0
    private lateinit var mediaPlayer: MediaPlayer
    private val chart = ArrayList<Int>()

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        vBlock = findViewById(R.id.flBlock)
        flBoard = findViewById(R.id.flBoard)
        vColumnLeft = findViewById(R.id.vColumnLeft)
        vColumnTwo = findViewById(R.id.vColumnTwo)
        vColumnThree = findViewById(R.id.vColumnThree)
        vColumnFour = findViewById(R.id.vColumnFour)
        vColumnRight = findViewById(R.id.vColumnRight)
        tvJudge = findViewById(R.id.tvJudge)
        tvCombbo = findViewById(R.id.tvCombo)
        ivComboCircle = findViewById(R.id.ivComboCircle)
        ivComboCirclePulse = findViewById(R.id.ivComboCirclePulse)

        windowWidth = flBoard.layoutParams.width
        val display = windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        height = size.y

        columnList = ArrayList()
        columnList.add(vColumnLeft)
        columnList.add(vColumnTwo)
        columnList.add(vColumnThree)
        columnList.add(vColumnFour)
        columnList.add(vColumnRight)

        for (column in columnList) {
            column.setOnClickListener {
                vBlock.tag = column.tag
                AdditiveAnimator
                        .animate(vBlock)
                        .setInterpolator(LinearInterpolator())
                        .x(column.x)
                        .setDuration(0)
                        .start()
            }
        }

        flBoard.setOnTouchListener(View.OnTouchListener { v, event ->
            when (event!!.getAction()) {

                MotionEvent.ACTION_DOWN -> {
                    dX = vBlock!!.getX() - event!!.getRawX()
                    if (!dXLock) {
                        dXInit = event!!.getRawX()
                        dXLock = true
                    }
                }

                MotionEvent.ACTION_UP -> {
                    dXLock = false
                }

                MotionEvent.ACTION_MOVE -> {

                    val width = size.x

                    if (isSwipeLeft(dXInit, event.rawX)) {
                        if (vBlock!!.x >= 0) {
                            vBlock!!.animate()
                                    .x(event!!.getRawX() + dX)
                                    .setDuration(0)
                                    .start()
                        }

                    } else {
                        if (vBlock!!.x <= width - vBlock.width) {
                            vBlock!!.animate()
                                    .x(event!!.getRawX() + dX)
                                    .setDuration(0)
                                    .start()
                        }
                    }

                }

                else -> return@OnTouchListener false
            }

            true
        })
    }

    private fun isSwipeLeft(dX1: Float, dX2: Float): Boolean {
        dXLock = false
        if (dX1 < dX2) {
            Log.e("swipe", "right")
            return false
        } else {
            Log.e("swipe", "left")
            return true
        }
    }

    private fun testTargets() {
        var t = 0
        var upAnte = false
        val handler = Handler()
        handler.postDelayed(object : Runnable {
            override fun run() {
                if (t < chart.size - 1) {
                    generateTarget(chart.get(t))
                    t++
                    handler.postDelayed(this, 1000)
                    if (t == 10) {
                        handler.postDelayed(this, 500)
                    }

                    if (t == 30) {
//                        handler.postDelayed(this, 200)
                    }
                }
            }
        }, 1000)
    }


    override fun onResume() {
        super.onResume()
        testTargets()
    }

    private fun generateTarget(chartPosition: Int) {
        val rand = Random()
        val randomNum = rand.nextInt(4 - 0)

        val column = columnList.get(chartPosition)
        Log.e("column number", "" + chartPosition)
        var targetBlock = TargetItem(this)
        targetBlock.minimumWidth = vBlock.width
        targetBlock.minimumHeight = vBlock.height
        targetBlock.tag = column.tag
        targetBlock.y = column.y
        column.addView(targetBlock)
        AdditiveAnimator
                .animate(targetBlock)
                .setInterpolator(LinearInterpolator())
                .y(height.toFloat())
                .addUpdateListener {
                    if (targetBlock.y >= height && vBlock.tag == targetBlock.tag) {
                        Log.e("collision", "detected")
                        targetBlock.visibility = View.GONE
                        registerHit()
                    } else if (targetBlock.y >= height && vBlock.tag != targetBlock.tag) {
                        targetBlock.visibility = View.GONE
                        registerMiss()
                    }
                }
                .setDuration(800)
                .alpha(1f)
                .start()
    }

    private fun registerHit() {
        tvJudge.text = "PERFECT"
        tvJudge.setTextColor(ContextCompat.getColor(this, R.color.perfectColour))
        AdditiveAnimator
                .animate(tvJudge)
                .setDuration(200)
                .setInterpolator(BounceInterpolator())
                .alpha(1f)
                .rotationBy(-10f)
                .scale(1.6f)
                .then()
                .setDuration(500)
                .alpha(0f)
                .rotationBy(10f)
                .scale(.6f)
                .start()
        vBlock.registerHit()
        score++
        tvCombbo.text = score.toString()
        if (score >= 5) {
            if (score % 10 == 0) {
                ivComboCirclePulse.alpha = 1f
                AdditiveAnimator
                        .animate(ivComboCirclePulse)
                        .setDuration(1000)
                        .setInterpolator(LinearInterpolator())
                        .alpha(0f)
                        .scale(1.8f)
                        .then()
                        .scale(1f)
                        .start()
            }
            AdditiveAnimator
                    .animate(tvCombbo)
                    .setDuration(500)
                    .setInterpolator(BounceInterpolator())
                    .alpha(1f)
                    .rotationBy(-10f)
                    .scale(1.3f)
                    .then()
                    .setDuration(500)
                    .rotationBy(10f)
                    .scale(1f)
                    .start()
            ivComboCircle.alpha = 1f
        }
        if (score < 5) {
            tvCombbo.alpha = 0f
        }
        Log.e("HIT!", " " + score + " combo!")
    }

    private fun registerMiss() {
        tvJudge.text = "MISS"
        tvJudge.setTextColor(ContextCompat.getColor(this, R.color.missColour))
        AdditiveAnimator
                .animate(tvJudge)
                .setDuration(200)
                .setInterpolator(BounceInterpolator())
                .alpha(1f)
                .scale(1.2f)
                .rotation(0.8f)
                .then()
                .setDuration(500)
                .alpha(0f)
                .scale(.8f)
                .start()
        tvCombbo.alpha = 0f
        ivComboCircle.alpha = 0f
        ivComboCirclePulse.alpha = 0f
        score = 0
        if (score < 5) {
            tvCombbo.alpha = 0f
        }
        Log.e("MISS!", " ")
    }

    override fun onStart() {
        super.onStart()
        setTestChart()
        loadTrack()
    }

    private fun loadTrack() {
        mediaPlayer = MediaPlayer.create(this, R.raw.reusenoise_resample)
        mediaPlayer.start()
    }

    private fun setTestChart() {
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(0)
        chart.add(1)
        chart.add(1)
        chart.add(1)
        chart.add(1)
        chart.add(1)
        chart.add(1)
        chart.add(1)
        chart.add(1)
        chart.add(1)
        chart.add(1)
        chart.add(1)
        chart.add(1)
        chart.add(1)
        chart.add(1)
        chart.add(1)
        chart.add(1)
        chart.add(1)
        chart.add(1)
        chart.add(1)
        chart.add(1)
        chart.add(1)
        chart.add(1)
        chart.add(1)
        chart.add(1)
        chart.add(1)
        chart.add(1)
        chart.add(1)
        chart.add(1)
        chart.add(1)
        chart.add(1)
        chart.add(1)
        chart.add(1)
        chart.add(1)
        chart.add(1)
        chart.add(1)
        chart.add(1)
        chart.add(2)
        chart.add(0)
        chart.add(3)
        chart.add(1)
        chart.add(1)
        chart.add(3)
        chart.add(2)
        chart.add(1)
        chart.add(0)
        chart.add(1)
        chart.add(2)
        chart.add(3)
        chart.add(2)
        chart.add(1)
        chart.add(0)
        chart.add(1)
        chart.add(2)
        chart.add(3)
        chart.add(2)
        chart.add(1)
        chart.add(0)
        chart.add(1)
        chart.add(2)
        chart.add(3)
        chart.add(2)
        chart.add(1)
        chart.add(0)
        chart.add(1)
        chart.add(0)
        chart.add(3)
        chart.add(2)
        chart.add(0)
        chart.add(1)
        chart.add(3)
        chart.add(1)
        chart.add(2)
        chart.add(2)
        chart.add(1)
        chart.add(3)
        chart.add(1)
        chart.add(1)
        chart.add(0)
        chart.add(1)
        chart.add(0)
        chart.add(1)
        chart.add(3)
        chart.add(1)
        chart.add(3)
        chart.add(2)
        chart.add(3)
        chart.add(3)
        chart.add(3)
        chart.add(2)
        chart.add(3)
        chart.add(1)
        chart.add(0)
        chart.add(1)
        chart.add(2)
        chart.add(3)
        chart.add(4)
        chart.add(0)
        chart.add(2)
        chart.add(3)
        chart.add(1)
        chart.add(1)
        chart.add(1)
        chart.add(2)
        chart.add(1)
        chart.add(1)
        chart.add(3)
        chart.add(1)
        chart.add(0)
    }

}


