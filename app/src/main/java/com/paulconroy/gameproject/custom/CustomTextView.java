package com.paulconroy.gameproject.custom;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;


/**
 * Custom Font EditText
 *
 * @author paulconroy
 * @since 06/11/2017
 **/
public class CustomTextView extends AppCompatTextView {

    private static Typeface typefaceInstance;

    /**
     * Default Constructor
     *
     * @param context Activity Context
     */
    public CustomTextView(Context context) {
        super(context);
        initView();
    }

    /**
     * Default Constructor
     *
     * @param context Activity Context
     * @param attrs   Layout Attributes
     */
    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    /**
     * Default Constructor
     *
     * @param context      Activity Context
     * @param attrs        Layout Attributes
     * @param defStyleAttr Style Attributes
     */
    public CustomTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    /**
     * Initialise View with Custom Font
     */
    private void initView() {
        if (typefaceInstance == null) {
            typefaceInstance = Typeface.createFromAsset(getContext().getAssets(), "fonts/abuket.otf");
        }

        setTypeface(typefaceInstance);
    }

}