package com.paulconroy.gameproject.custom;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.res.ResourcesCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.paulconroy.gameproject.R;


/**
 * Created by paulconroy on 23/05/2017.
 */

public class BombItem extends LinearLayout {

    private LayoutInflater layoutInflater;
    private TextView tvFunctionName;
    private ImageView ivFunctionImage;
    private Context context;
    private String function = "";

    public BombItem(Context context) {
        super(context);
        layoutInflater = LayoutInflater.from(context);
        init(context);
    }

    public BombItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        layoutInflater = LayoutInflater.from(context);
        init(context);
    }

    public BombItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        layoutInflater = LayoutInflater.from(context);
        init(context);
    }

    public void init(@NonNull final Context context) {
        View view = layoutInflater.inflate(R.layout.layout_target, this, true);
        this.context = context;
    }

    public void setFunctionName(@NonNull final String functionName) {
        tvFunctionName.setText(functionName.toUpperCase());
    }

    public void setIvFunctionImage(final int functionImage) {
        if (functionImage != 0) {
            ivFunctionImage.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(),
                    functionImage, null));
        }
    }

    public void setFunction(@NonNull final String function) {
        this.function = function;
    }

    public String getFunction() {
        return this.function;
    }

    public TextView getTvFunctionName() {
        return tvFunctionName;
    }

    public ImageView getIvFunctionImage() {
        return ivFunctionImage;
    }
}
