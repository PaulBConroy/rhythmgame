package com.paulconroy.gameproject.custom;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.res.ResourcesCompat;
import android.util.AttributeSet;
import android.util.FloatProperty;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.BounceInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.paulconroy.gameproject.R;

import at.wirecube.additiveanimations.additive_animator.AdditiveAnimator;
import at.wirecube.additiveanimations.helper.evaluators.ColorEvaluator;


/**
 * Created by paulconroy on 23/05/2017.
 */

public class BlockItem extends LinearLayout {

    private LayoutInflater layoutInflater;
    private Context context;
    private String function = "";
    private View vBlockAnimator;

    public BlockItem(Context context) {
        super(context);
        layoutInflater = LayoutInflater.from(context);
        init(context);
    }

    public BlockItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        layoutInflater = LayoutInflater.from(context);
        init(context);
    }

    public BlockItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        layoutInflater = LayoutInflater.from(context);
        init(context);
    }

    public void init(@NonNull final Context context) {
        View view = layoutInflater.inflate(R.layout.layout_block, this, true);
        this.context = context;
        vBlockAnimator = view.findViewById(R.id.vBlockAnimator);

    }

    public void registerHit() {
        Log.e("Animation pulse", "start");
//        Animation animation = AnimationUtils.loadAnimation(this.context, R.anim.pulse_onboard);
//        vBlockAnimator.setAnimation(animation);
//        animation.startNow();
        AdditiveAnimator.animate(vBlockAnimator).setDuration(100).alpha(1f).then().setDuration(100).alpha(0f).start();
    }
    public void setFunction(@NonNull final String function) {
        this.function = function;
    }

    public String getFunction() {
        return this.function;
    }
}
